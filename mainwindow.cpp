#include "mainwindow.h"
#include "ui_mainwindow.h"


/* void output(const Json::Value & value)
{
    // querying the json object is very simple
    std::cout << value["hello"];
    std::cout << value["number"];
    std::cout << value["array"][0] << value["array"][1];
    std::cout << value["object"]["hello"];
}

void        test_json()
{
    Json::Value fromScratch;
    Json::Value array;

    array.append("hello");
    array.append("world");

    fromScratch["hello"] = "world";
    fromScratch["number"] = 2;
    fromScratch["array"] = array;
    fromScratch["object"]["hello"] = "world";
    fromScratch["object"]["test"] = "bonjour test";

    //output(fromScratch);

    // write in a nice readible way
    Json::StyledWriter styledWriter;
    //std::cout << styledWriter.write(fromScratch);

    // ---- parse from string ----

    // write in a compact way
    //Json::FastWriter fastWriter;
    //std::string jsonMessage = fastWriter.write(fromScratch);

    std::string jsonMessage = styledWriter.write(fromScratch);
    //std::cout << jsonMessage << std::endl;

    QFile   testResult;
    testResult.setFileName("testResult.json");
    if (!testResult.open(QIODevice::ReadWrite | QIODevice::Text))
        qDebug() << "PAS REUSSI";
    QTextStream out(&testResult);
    out << jsonMessage.c_str() << "\n";
    testResult.close();



    Json::Value parsedFromString;
    std::ifstream file("testResult.json", std::ifstream::binary);
    file >> parsedFromString;

    std::string result;
    Json::Reader reader;
    bool parsingSuccessful = reader.parse(result, parsedFromString, false);
    if (!parsingSuccessful)
    {
        std::cout << "Error: " << reader.getFormatedErrorMessages() << "\n";
    }
    std::cout << "After error: " << styledWriter.write(parsedFromString) << std::endl;
} */

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->currentVecIndex = -1;
    this->ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->tableWidget->setRowCount(0);
    this->ui->actionSupprimer->setDisabled(true);
    this->ui->actionModifier->setDisabled(true);

    this->ui->editGroupBox->hide();

    this->add = QPixmap(":/anicons/icons/add.png");
    this->del = QPixmap(":/anicons/icons/delete.png");

    this->ui->historicTable->setVerticalHeaderItem(0, new QTableWidgetItem(this->add, ""));
    connect(this->ui->historicTable->verticalHeader(), SIGNAL(sectionClicked(int)), this, SLOT(verticalHeaderClicked(int)));

    this->readFile();
}

MainWindow::~MainWindow()
{
    delete ui;
    for (int i = 0; i < this->vecAnimal.size(); ++i)
        delete this->vecAnimal[i];
    this->vecAnimal.clear();
}





/*
 *      JSON
 */

void            MainWindow::readFile()
{
    Json::Value     root;
    Json::Reader    reader;
    std::ifstream   animalFile("animals.json", std::ifstream::binary);
    bool            parsingSuccessful = reader.parse(animalFile, root);

    if (!parsingSuccessful)
    {
        std::cout << "Error: " << reader.getFormattedErrorMessages() << "\n";
        return ;
    }

    Json::Value     list = root["List of names"];
    for (unsigned int i = 0; i < list.size(); ++i)
    {
        std::string name = list.get(i, "NONE").asString();
        std::string species = root[name].get("Species", "Non défini").asString();
        std::string sexe = root[name].get("Sexe", "Non défini").asString();
        std::string age = root[name].get("Age", "0").asString();
        std::string stralive = root[name].get("Alive", "true").asString();
        std::string date = root[name].get("Date", "NONE").asString();
        bool        alive;
        if (stralive.compare("true") == 0)
            alive = true;
        else
            alive = false;

        Animal      *animal = new Animal(species.c_str(), name.c_str(), sexe.c_str(), age.c_str(), QDate::fromString(date.c_str(), "dd.MM.yyyy"), alive);

        Json::Value vecHistory = root[name]["Infos"];
        for (unsigned int n = 0; n < vecHistory.size(); ++n)
            animal->addHistoric(vecHistory[n][0].asString().c_str(), vecHistory[n][1].asString().c_str());

        this->addLine(animal, true);
    }
}

void            MainWindow::writeToFile(Animal *animal)
{
    Json::Value         newEntry;
    Json::Reader        reader;
    Json::Value         infosHistoric;
    Json::Value         names;
    Json::StyledWriter  styledWriter;
    std::string         jsonToStr;
    QFile               animalFile("animals.json");
    std::ifstream       file("animals.json", std::ifstream::binary);

    reader.parse(file, newEntry);

    names = newEntry["List of names"];
    names.append(animal->getName()->toStdString());
    newEntry["List of names"] = names;
    newEntry[animal->getName()->toStdString()]["Species"] = animal->getSpecies()->toStdString();
    newEntry[animal->getName()->toStdString()]["Sexe"] = animal->getSexe()->toStdString();
    newEntry[animal->getName()->toStdString()]["Age"] = animal->getAge()->toStdString();
    newEntry[animal->getName()->toStdString()]["Date"] = animal->getDate()->toString("dd.MM.yyyy").toStdString();
    newEntry[animal->getName()->toStdString()]["Alive"] = animal->getAlive();

    for (int i = 0; i < animal->getHistoric().size(); ++i)
    {
        Json::Value     array;
        array.append(animal->getHistoric()[i].date->toStdString());
        array.append(animal->getHistoric()[i].info->toStdString());
        infosHistoric.append(array);
    }
    newEntry[animal->getName()->toStdString()]["Infos"] = infosHistoric;
    jsonToStr = styledWriter.write(newEntry);
    if (!animalFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
        qDebug() << "Couldn't open animalFile";
    else
    {
        QTextStream out(&animalFile);
        out << jsonToStr.c_str() << "\n";
        animalFile.close();
    }
}

void            MainWindow::writeAllToFile()
{
    Json::Value         newEntry;
    Json::Value         infosHistoric;
    Json::Value         names;
    Json::StyledWriter  styledWriter;
    std::string         jsonToStr;
    QFile               animalFile("animals.json");

    for (int i = 0; i < this->vecAnimal.size(); ++i)
    {
        names.append(this->vecAnimal[i]->getName()->toStdString());
        newEntry[this->vecAnimal[i]->getName()->toStdString()]["Species"] = this->vecAnimal[i]->getSpecies()->toStdString();
        newEntry[this->vecAnimal[i]->getName()->toStdString()]["Sexe"] = this->vecAnimal[i]->getSexe()->toStdString();
        newEntry[this->vecAnimal[i]->getName()->toStdString()]["Age"] = this->vecAnimal[i]->getAge()->toStdString();
        newEntry[this->vecAnimal[i]->getName()->toStdString()]["Date"] = this->vecAnimal[i]->getDate()->toString("dd.MM.yyyy").toStdString();
        newEntry[this->vecAnimal[i]->getName()->toStdString()]["Alive"] = this->vecAnimal[i]->getAlive();
        infosHistoric.clear();
        for (int n = 0; n < this->vecAnimal[i]->getHistoric().size(); ++n)
        {
            Json::Value     array;
            array.append(this->vecAnimal[i]->getHistoric()[n].date->toStdString());
            array.append(this->vecAnimal[i]->getHistoric()[n].info->toStdString());
            infosHistoric.append(array);
        }
        newEntry[this->vecAnimal[i]->getName()->toStdString()]["Infos"] = infosHistoric;
    }
    newEntry["List of names"] = names;
    jsonToStr = styledWriter.write(newEntry);
    if (!animalFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
        qDebug() << "Couldn't open animalFile";
    else
    {
        QTextStream out(&animalFile);
        out << jsonToStr.c_str() << "\n";
        animalFile.close();
    }
}

/*
 *              !JSON
 */





/*
 *              PUBLIC FUNCTIONS
 */

QIcon           MainWindow::define_iconPath(QString *species)
{
    QString     speciesTab[6] = { "Chat", "Chien", "Lapin", "Cobaye", "Tortue", "Perroquet" };
    QString     speciesIcon[6] = { "cat_male.png", "dog.png", "rabbit.png", "guinea.png", "turtle.png", "parrot.png" };

    for (int i = 0; i < 6; ++i)
    {
        if (species->compare(speciesTab[i]) == 0)
            return (QIcon(QPixmap(":/anicons/icons/" + speciesIcon[i])));
    }
    return (QIcon(QPixmap(":/anicons/icons/alien.png")));
}

void            MainWindow::addLine(Animal *animal, bool readMode)
{
    this->ui->tableWidget->setRowCount(this->ui->tableWidget->rowCount() + 1);
    this->ui->tableWidget->setCurrentCell(-1, -1);

    QTableWidgetItem    *widgetSpecies = new QTableWidgetItem(define_iconPath(animal->getSpecies()), "");
    QTableWidgetItem    *widgetName = new QTableWidgetItem(*animal->getName());
    QTableWidgetItem    *widgetSexe = new QTableWidgetItem(*animal->getSexe());
    QTableWidgetItem    *widgetAge = new QTableWidgetItem(*animal->getAge());
    QTableWidgetItem    *widgetDate = new QTableWidgetItem(animal->getDate()->toString("dd.MM.yyyy"));

    this->ui->tableWidget->setVerticalHeaderItem(this->ui->tableWidget->rowCount() - 1, widgetSpecies);
    this->ui->tableWidget->setItem(this->ui->tableWidget->rowCount() - 1, 0, widgetName);
    this->ui->tableWidget->setItem(this->ui->tableWidget->rowCount() - 1, 1, widgetSexe);
    this->ui->tableWidget->setItem(this->ui->tableWidget->rowCount() - 1, 2, widgetAge);
    this->ui->tableWidget->setItem(this->ui->tableWidget->rowCount() - 1, 3, widgetDate);

    this->vecAnimal.append(animal);

    if (!readMode)
        writeToFile(animal);
}

void            MainWindow::editLine(int row, Animal *animal)
{
    this->ui->tableWidget->verticalHeaderItem(row)->setIcon(define_iconPath(animal->getSpecies()));
    this->ui->tableWidget->item(row, 0)->setText(*animal->getName());
    this->ui->tableWidget->item(row, 1)->setText(*animal->getSexe());
    this->ui->tableWidget->item(row, 2)->setText(*animal->getAge());
    this->ui->tableWidget->item(row, 3)->setText(animal->getDate()->toString("dd.MM.yyyy"));

    writeAllToFile();
}

/*
 *              !PUBLIC FUNCTIONS
 */





/*
 *              SLOTS
 */

void            MainWindow::on_actionAjouter_triggered()
{
    addAnimal   addAnimalWindow;
    int         dialogReturn = addAnimalWindow.exec();

    if (dialogReturn == QDialog::Accepted)
        this->addLine(addAnimalWindow.getAnimal(), false);
}

void            MainWindow::on_actionSupprimer_triggered()
{
    delete this->vecAnimal[this->ui->tableWidget->currentRow()];
    this->vecAnimal.remove(this->ui->tableWidget->currentRow());

    this->ui->tableWidget->removeRow(this->ui->tableWidget->currentRow());
    this->ui->tableWidget->setCurrentCell(-1, -1);

    this->writeAllToFile();
}

void            MainWindow::on_actionModifier_triggered()
{
    this->ui->tableWidget->cellActivated(this->ui->tableWidget->currentRow(), this->ui->tableWidget->currentColumn());
}

void            MainWindow::on_tableWidget_itemSelectionChanged()
{
    if (this->ui->tableWidget->currentRow() < 0 || this->ui->tableWidget->currentRow() > this->ui->tableWidget->rowCount())
    {
        this->ui->actionSupprimer->setDisabled(true);
        this->ui->actionModifier->setDisabled(true);
    }
    else if (!this->ui->actionSupprimer->isEnabled() && this->ui->editGroupBox->isHidden())
    {
        this->ui->actionSupprimer->setEnabled(true);
        this->ui->actionModifier->setEnabled(true);
    }
}

void            MainWindow::on_tableWidget_cellActivated(int row, int column)
{
    column = column;

    this->currentVecIndex = row;
    this->ui->speciesComboBox->setCurrentText(*this->vecAnimal[row]->getSpecies());
    this->ui->nomLineEdit->setText(*this->vecAnimal[row]->getName());
    this->ui->sexeComboBox->setCurrentText(*this->vecAnimal[row]->getSexe());
    this->ui->ageSpinBox->setValue(this->vecAnimal[row]->getAge()->toInt());
    this->ui->dateEdit->setDate(*this->vecAnimal[row]->getDate());

    QVector<s_historic>     vec = this->vecAnimal[row]->getHistoric();
    this->ui->historicTable->setRowCount(0);
    this->ui->historicTable->setRowCount(vec.size() + 1);
    for (int i = 0; i < vec.size(); ++i)
    {
        QTableWidgetItem    *item = new QTableWidgetItem(this->del, "");
        item->setCheckState(Qt::Checked);
        this->ui->historicTable->setVerticalHeaderItem(i, item);
        this->ui->historicTable->setItem(i, 0, new QTableWidgetItem(*vec[i].date));
        this->ui->historicTable->setItem(i, 1, new QTableWidgetItem(*vec[i].info));
    }

    QTableWidgetItem        *lastLine = new QTableWidgetItem(this->add, "");
    lastLine->setCheckState(Qt::Unchecked);
    this->ui->historicTable->setVerticalHeaderItem(vec.size(), lastLine);

    this->ui->editGroupBox->show();
    this->ui->actionSupprimer->setDisabled(true);
}

void            MainWindow::verticalHeaderClicked(int index)
{
    if (this->ui->historicTable->verticalHeaderItem(index)->checkState() == Qt::Unchecked)
    {
        this->ui->historicTable->verticalHeaderItem(index)->setIcon(this->del);
        this->ui->historicTable->verticalHeaderItem(index)->setCheckState(Qt::Checked);

        this->ui->historicTable->setRowCount(this->ui->historicTable->rowCount() + 1);
        this->ui->historicTable->setVerticalHeaderItem(index + 1, new QTableWidgetItem(this->add, ""));
    }
    else if (this->ui->historicTable->verticalHeaderItem(index)->checkState() == Qt::Checked)
        this->ui->historicTable->removeRow(index);
}

void            MainWindow::on_historicTable_cellDoubleClicked(int row, int column)
{
    column = column;

    if (this->ui->historicTable->verticalHeaderItem(row)->checkState() == Qt::Unchecked)
        this->ui->historicTable->verticalHeader()->sectionClicked(row);
}

void            MainWindow::on_acceptButton_clicked()
{
    Animal      *animal = this->vecAnimal[this->currentVecIndex];

    animal->setSpecies(this->ui->speciesComboBox->currentText());
    animal->setName(this->ui->nomLineEdit->text());
    animal->setSexe(this->ui->sexeComboBox->currentText());
    animal->setAge(this->ui->ageSpinBox->text());
    animal->setDate(this->ui->dateEdit->date());

    animal->deleteHistoric();
    for (int i = 0; i < this->ui->historicTable->rowCount(); ++i)
    {
        if (this->ui->historicTable->item(i, 0) != 0 && this->ui->historicTable->item(i, 1) != 0)
            animal->addHistoric(this->ui->historicTable->item(i, 0)->text(), this->ui->historicTable->item(i, 1)->text());
    }
    this->ui->editGroupBox->hide();
    this->ui->actionSupprimer->setEnabled(true);

    this->editLine(this->currentVecIndex, animal);
}

void            MainWindow::on_cancelButton_clicked()
{
    this->ui->editGroupBox->hide();
    this->ui->actionSupprimer->setEnabled(true);
}
/*
 *              !SLOTS
 */


