#-------------------------------------------------
#
# Project created by QtCreator 2016-03-17T18:13:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AnimalManager
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    addanimal.cpp \
    animal.cpp \
    jsoncpp.cpp

HEADERS  += mainwindow.h \
    addanimal.h \
    animal.h \
    json/json-forwards.h \
    json/json.h

FORMS    += mainwindow.ui \
    addanimal.ui

RESOURCES += \
    animalmanagericons.qrc

DISTFILES += \
    icon.rc

win32:RC_FILE += icon.rc

#INCLUDEPATH += D:/Projets/jsoncpp-master/jsoncpp-master/include
