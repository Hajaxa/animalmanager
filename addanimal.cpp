#include "addanimal.h"
#include "ui_addanimal.h"

addAnimal::addAnimal(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addAnimal)
{
    ui->setupUi(this);
    this->animal = NULL;
}

addAnimal::~addAnimal()
{
    delete ui;
}

Animal  *addAnimal::getAnimal()
{
    return (this->animal);
}

void    addAnimal::on_buttonBox_accepted()
{
    this->animal = new Animal(this->ui->speciesComboBox->currentText(), this->ui->nameLineEdit->text(), this->ui->sexeComboBox->currentText(),
                              this->ui->ageSpinBox->text(), this->ui->dateEdit->date(), this->ui->aliveCheckBox->isChecked());
    //this->animal->displayContent();
    addAnimal::accept();
}

void    addAnimal::on_buttonBox_rejected()
{
    addAnimal::reject();
}
