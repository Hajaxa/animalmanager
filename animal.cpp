#include "animal.h"

Animal::Animal(QString _species, QString _name, QString _sexe, QString _age, QDate _date, bool _alive)
{
    this->species = new QString(_species);
    this->name = new QString(_name);
    this->sexe = new QString(_sexe);
    this->age = new QString(_age);
    this->date = new QDate(_date);
    this->alive = _alive;
}

Animal::~Animal()
{
    delete this->species;
    delete this->name;
    delete this->sexe;
    delete this->age;
    delete this->date;
    this->deleteHistoric();
}

void    Animal::displayContent()
{
    qDebug() << *this->species << *this->name << *this->sexe << *this->age << *this->date << this->alive;
}

void    Animal::setSpecies(QString _species)
{
    delete this->species;

    this->species = new QString(_species);
}

void    Animal::setName(QString _name)
{
    delete this->name;

    this->name = new QString(_name);
}

void    Animal::setSexe(QString _sexe)
{
    delete this->sexe;

    this->sexe = new QString(_sexe);
}

void    Animal::setAge(QString _age)
{
    delete this->age;

    this->age = new QString(_age);
}

void    Animal::setDate(QDate _date)
{
    delete this->date;

    this->date = new QDate(_date);
}

void    Animal::setAlive(bool _alive)
{
    this->alive = _alive;
}

void    Animal::addHistoric(QString _date, QString _info)
{
    s_historic  newLine;

    newLine.date = new QString(_date);
    newLine.info = new QString(_info);

    this->vecHistoric.append(newLine);
}


void    Animal::deleteHistoric()
{
    for (int i = 0; i < this->vecHistoric.size(); ++i)
    {
        delete this->vecHistoric[i].date;
        delete this->vecHistoric[i].info;
    }
    this->vecHistoric.clear();
}

QString *Animal::getSpecies()
{
    return (this->species);
}

QString *Animal::getName()
{
    return (this->name);
}

QString *Animal::getSexe()
{
    return (this->sexe);
}

QString *Animal::getAge()
{
    return (this->age);
}

QDate   *Animal::getDate()
{
    return (this->date);
}

bool    Animal::getAlive()
{
    return (this->alive);
}

QVector<s_historic> Animal::getHistoric()
{
    return (this->vecHistoric);
}
