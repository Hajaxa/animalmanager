#ifndef ANIMAL_H
#define ANIMAL_H

#include <QString>
#include <QDate>
#include <QDebug>

typedef struct  t_historic
{
    QString     *date;
    QString     *info;
}               s_historic;

class Animal
{
public:
    Animal(QString _species, QString _name, QString _sexe, QString _age, QDate _date, bool _alive);
    ~Animal();

    void                displayContent();

    void                setSpecies(QString _species);
    void                setName(QString _name);
    void                setSexe(QString _sexe);
    void                setAge(QString _age);
    void                setDate(QDate _date);
    void                setAlive(bool _alive);
    void                addHistoric(QString _date, QString _info);

    void                deleteHistoric();

    QString             *getSpecies();
    QString             *getName();
    QString             *getSexe();
    QString             *getAge();
    QDate               *getDate();
    bool                getAlive();
    QVector<s_historic> getHistoric();


private:
    QString             *species;
    QString             *name;
    QString             *sexe;
    QString             *age;
    QDate               *date;
    bool                alive;
    QVector<s_historic> vecHistoric;
};

#endif // ANIMAL_H
