#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QVector>
#include <QLabel>
#include <QFile>
#include <QTableWidgetItem>
#include <iostream>
#include <fstream>
#include <json/json.h>
#include <json/json-forwards.h>
#include "addanimal.h"

/*
 * AUTHOR: VAZ DE PINHO Sébastien
 */

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QIcon               define_iconPath(QString *species);
    void                addLine(Animal *animal, bool readMode);
    void                editLine(int row, Animal *animal);

    void                readFile();                         // Read JSON file containing Animal informations
    void                writeToFile(Animal *animal);        // Write Animal informations to JSON file (keeps file's text and append)
    void                writeAllToFile();                   // Write Animal informations to JSON file (re-write all informations)

private slots:
    void                on_actionAjouter_triggered();
    void                on_actionSupprimer_triggered();
    void                on_actionModifier_triggered();

    void                on_tableWidget_itemSelectionChanged();
    void                on_tableWidget_cellActivated(int row, int column);

    void                verticalHeaderClicked(int index);
    void                on_historicTable_cellDoubleClicked(int row, int column);

    void                on_acceptButton_clicked();
    void                on_cancelButton_clicked();

private:
    Ui::MainWindow      *ui;
    QVector<Animal*>    vecAnimal;                          // Vector containing all animals
    QIcon               add;                                // Add icon
    QIcon               del;                                // Delete icon
    int                 currentVecIndex;                    // Prevent error from clicking an other animal while being in edit mode
};

#endif // MAINWINDOW_H
