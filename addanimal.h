#ifndef ADDANIMAL_H
#define ADDANIMAL_H

#include <QDialog>
#include "animal.h"

namespace Ui {
class addAnimal;
}

class addAnimal : public QDialog
{
    Q_OBJECT

public:
    explicit addAnimal(QWidget *parent = 0);
    ~addAnimal();

    Animal          *getAnimal();

private slots:
    void            on_buttonBox_accepted();
    void            on_buttonBox_rejected();

private:
    Ui::addAnimal   *ui;
    Animal          *animal;
};

#endif // ADDANIMAL_H
